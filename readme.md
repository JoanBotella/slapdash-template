# Slapdash Template

A slapdash project template for quick implementations.

## License

GNU GPL v3 or later <https://www.gnu.org/licenses/gpl-3.0.en.html>

## Author

Joan Botella Vinaches <https://joanbotella.com>
