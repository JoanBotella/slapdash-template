<?php
declare(strict_types=1);

require __DIR__.'/../composer/vendor/autoload.php';

use template\library\configuration\TemplateConfiguration;
use slapdash\core\service\configurationContainer\CoreConfigurationContainer;

CoreConfigurationContainer::set(
	new TemplateConfiguration()
);