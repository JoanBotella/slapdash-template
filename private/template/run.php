<?php
declare(strict_types=1);

require __DIR__.'/setup.php';

use slapdash\core\service\app\CoreApp;

CoreApp::run();