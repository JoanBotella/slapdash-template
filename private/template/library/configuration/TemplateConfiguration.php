<?php
declare(strict_types=1);

namespace template\library\configuration;

use slapdash\demo\page\about\library\route\DemoAboutRoute;
use slapdash\demo\page\badRequest\library\route\DemoBadRequestRoute;
use slapdash\demo\page\greet\library\route\DemoGreetRoute;
use slapdash\demo\page\home\library\route\DemoHomeRoute;
use slapdash\demo\page\notFound\library\route\DemoNotFoundRoute;
use slapdash\core\library\configuration\CoreConfigurationAbs;
use slapdash\core\library\CoreHttpProtocolConstant;

final class TemplateConfiguration extends CoreConfigurationAbs
{

	public static function getAppName():string
	{
		return 'Template';
	}

	public static function getAppDirectoryName():string
	{
		return 'template';
	}

	public static function getAppCode():string
	{
		return 'template';
	}

	public static function getAppUrlProtocol():string
	{
		return CoreHttpProtocolConstant::HTTP;
	}

	public static function getAppUrlHost():string
	{
		return 'localhost';
	}

	public static function getAppUrlBasePath():string
	{
		return 'slapdash-template/repo';
	}

	public static function getRoutes():array
	{
		return [
			new DemoGreetRoute(),
			new DemoHomeRoute(),
			new DemoAboutRoute(),
			new DemoBadRequestRoute(),
			new DemoNotFoundRoute(),
		];
	}

}