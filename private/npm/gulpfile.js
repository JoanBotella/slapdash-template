'use strict';

const
	gulp = require('gulp'),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	minifyJs = require('gulp-terser'),
	uglifyCss = require('gulp-csso'),
	del = require('del'),

	projectDir = '../..',

		privateDir = projectDir + '/private',

			privateSlapdashDir = privateDir + '/slapdash',

				privateSlapdashCoreDir = privateSlapdashDir + '/core',

					privateSlapdashCoreAssetDir = privateSlapdashCoreDir + '/asset',

						privateSlapdashCoreAssetCssDir = privateSlapdashCoreAssetDir + '/css',
						privateSlapdashCoreAssetImgDir = privateSlapdashCoreAssetDir + '/img',
						privateSlapdashCoreAssetJsDir = privateSlapdashCoreAssetDir + '/js',

				privateSlapdashDemoDir = privateSlapdashDir + '/demo',

					privateSlapdashDemoAssetDir = privateSlapdashDemoDir + '/asset',

						privateSlapdashDemoAssetCssDir = privateSlapdashDemoAssetDir + '/css',
						privateSlapdashDemoAssetImgDir = privateSlapdashDemoAssetDir + '/img',
						privateSlapdashDemoAssetJsDir = privateSlapdashDemoAssetDir + '/js',

					privateSlapdashDemoPageDir = privateSlapdashDemoDir + '/page',

						privateSlapdashDemoPageAboutAssetDir = privateSlapdashDemoPageDir + '/about/asset',

						privateSlapdashDemoPageAboutAssetCssDir = privateSlapdashDemoPageAboutAssetDir + '/css',
						privateSlapdashDemoPageAboutAssetImgDir = privateSlapdashDemoPageAboutAssetDir + '/img',
						privateSlapdashDemoPageAboutAssetJsDir = privateSlapdashDemoPageAboutAssetDir + '/js',

					privateSlapdashDemoPageBadRequestAssetDir = privateSlapdashDemoPageDir + '/badRequest/asset',

						privateSlapdashDemoPageBadRequestAssetCssDir = privateSlapdashDemoPageBadRequestAssetDir + '/css',
						privateSlapdashDemoPageBadRequestAssetImgDir = privateSlapdashDemoPageBadRequestAssetDir + '/img',
						privateSlapdashDemoPageBadRequestAssetJsDir = privateSlapdashDemoPageBadRequestAssetDir + '/js',

					privateSlapdashDemoPageGreetAssetDir = privateSlapdashDemoPageDir + '/greet/asset',

						privateSlapdashDemoPageGreetAssetCssDir = privateSlapdashDemoPageGreetAssetDir + '/css',
						privateSlapdashDemoPageGreetAssetImgDir = privateSlapdashDemoPageGreetAssetDir + '/img',
						privateSlapdashDemoPageGreetAssetJsDir = privateSlapdashDemoPageGreetAssetDir + '/js',

					privateSlapdashDemoPageHomeAssetDir = privateSlapdashDemoPageDir + '/home/asset',

						privateSlapdashDemoPageHomeAssetCssDir = privateSlapdashDemoPageHomeAssetDir + '/css',
						privateSlapdashDemoPageHomeAssetImgDir = privateSlapdashDemoPageHomeAssetDir + '/img',
						privateSlapdashDemoPageHomeAssetJsDir = privateSlapdashDemoPageHomeAssetDir + '/js',

					privateSlapdashDemoPageNotFoundAssetDir = privateSlapdashDemoPageDir + '/notFound/asset',

						privateSlapdashDemoPageNotFoundAssetCssDir = privateSlapdashDemoPageNotFoundAssetDir + '/css',
						privateSlapdashDemoPageNotFoundAssetImgDir = privateSlapdashDemoPageNotFoundAssetDir + '/img',
						privateSlapdashDemoPageNotFoundAssetJsDir = privateSlapdashDemoPageNotFoundAssetDir + '/js',

			privateTemplateDir = privateDir + '/template',

				privateTemplateAssetDir = privateTemplateDir + '/asset',

					privateTemplateAssetCssDir = privateTemplateAssetDir + '/css',
					privateTemplateAssetImgDir = privateTemplateAssetDir + '/img',
					privateTemplateAssetJsDir = privateTemplateAssetDir + '/js',

		publicDir = projectDir + '/public',

			publicCssDir = publicDir + '/css',
			publicImgDir = publicDir + '/img',
			publicJsDir = publicDir + '/js'
;

const deleteDir = function (dir)
{
	del.sync(
		[
			dir + '/**',
		],
		{
			force: true
		}
	);
}

function css(done)
{
	deleteDir(publicCssDir);

	gulp
		.src([
			privateSlapdashCoreAssetCssDir + '/head.css',

			privateSlapdashDemoAssetCssDir + '/style.css',
				privateSlapdashDemoPageAboutAssetCssDir + '/style.css',
				privateSlapdashDemoPageBadRequestAssetCssDir + '/style.css',
				privateSlapdashDemoPageGreetAssetCssDir + '/style.css',
				privateSlapdashDemoPageHomeAssetCssDir + '/style.css',
				privateSlapdashDemoPageNotFoundAssetCssDir + '/style.css',

			privateTemplateAssetCssDir + '/style.css',
		])
		.pipe(concat('style.css'))
		.pipe(gulp.dest(publicCssDir))
		.pipe(rename('style.min.css'))
		.pipe(uglifyCss())
		.pipe(gulp.dest(publicCssDir))
	;

	done();
}

function img(done)
{
	deleteDir(publicImgDir);

	gulp
		.src([
			privateSlapdashCoreAssetImgDir + '/**',

			privateSlapdashDemoAssetImgDir + '/**',
				privateSlapdashDemoPageAboutAssetImgDir + '/**',
				privateSlapdashDemoPageBadRequestAssetImgDir + '/**',
				privateSlapdashDemoPageGreetAssetImgDir + '/**',
				privateSlapdashDemoPageHomeAssetImgDir + '/**',
				privateSlapdashDemoPageNotFoundAssetImgDir + '/**',

			privateTemplateAssetImgDir + '/**',
		])
		.pipe(gulp.dest(publicImgDir))
	;

	done();
}

function js(done)
{
	deleteDir(publicJsDir);

	gulp
		.src([
			privateSlapdashCoreAssetJsDir + '/head.js',

			privateSlapdashDemoAssetJsDir + '/script.js',
				privateSlapdashDemoPageAboutAssetJsDir + '/script.js',
				privateSlapdashDemoPageBadRequestAssetJsDir + '/script.js',
				privateSlapdashDemoPageGreetAssetJsDir + '/script.js',
				privateSlapdashDemoPageHomeAssetJsDir + '/script.js',
				privateSlapdashDemoPageNotFoundAssetJsDir + '/script.js',

			privateTemplateAssetJsDir + '/script.js',

			privateSlapdashCoreAssetJsDir + '/route.js',
			privateSlapdashCoreAssetJsDir + '/tail.js',
		])
		.pipe(concat('script.js'))
		.pipe(gulp.dest(publicJsDir))
		.pipe(rename('script.min.js'))
		.pipe(minifyJs())
		.pipe(gulp.dest(publicJsDir))
	;

	done();
}

gulp.watch(
	[
		privateSlapdashCoreAssetCssDir,

		privateSlapdashDemoAssetCssDir,
			privateSlapdashDemoPageAboutAssetCssDir,
			privateSlapdashDemoPageBadRequestAssetCssDir,
			privateSlapdashDemoPageGreetAssetCssDir,
			privateSlapdashDemoPageHomeAssetCssDir,
			privateSlapdashDemoPageNotFoundAssetCssDir,

		privateTemplateAssetCssDir,
	],
	{
	},
	gulp.parallel(
		css
	)
);

gulp.watch(
	[
		privateSlapdashCoreAssetImgDir,

		privateSlapdashDemoAssetImgDir,
			privateSlapdashDemoPageAboutAssetImgDir,
			privateSlapdashDemoPageBadRequestAssetImgDir,
			privateSlapdashDemoPageGreetAssetImgDir,
			privateSlapdashDemoPageHomeAssetImgDir,
			privateSlapdashDemoPageNotFoundAssetImgDir,

		privateTemplateAssetImgDir,
	],
	{
	},
	gulp.parallel(
		img
	)
);

gulp.watch(
	[
		privateSlapdashCoreAssetJsDir,

		privateSlapdashDemoAssetJsDir,
			privateSlapdashDemoPageAboutAssetJsDir,
			privateSlapdashDemoPageBadRequestAssetJsDir,
			privateSlapdashDemoPageGreetAssetJsDir,
			privateSlapdashDemoPageHomeAssetJsDir,
			privateSlapdashDemoPageNotFoundAssetJsDir,

		privateTemplateAssetJsDir,
	],
	{
	},
	gulp.parallel(
		js
	)
);

exports.default = gulp.parallel(
	css,
	img,
	js
);
