
const route = function (appCode)
{
	const root = document.querySelector('[data-app="' + appCode + '"]');

	if (root === undefined)
	{
		return;
	}

	const pageCode = root.getAttribute('data-page');

	if (pageCode === undefined)
	{
		throw 'data-page attribute not found!';
	}

	if (typeof pageSetuppers[pageCode] === 'function')
	{
		pageSetuppers[pageCode]();
	}
}