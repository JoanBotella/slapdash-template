<?php
declare(strict_types=1);

namespace slapdash\core\library\controller;

interface CoreControllerItf
{

	public function action():void;

}