<?php
declare(strict_types=1);

namespace slapdash\core\library\controller;

use slapdash\core\library\controller\CoreControllerItf;

abstract class CoreControllerAbs implements CoreControllerItf
{
}