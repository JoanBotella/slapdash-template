<?php
declare(strict_types=1);

namespace slapdash\core\library\widget;

interface CoreWidgetItf
{

	public function render(array $context):string;

}