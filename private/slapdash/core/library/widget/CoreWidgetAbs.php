<?php
declare(strict_types=1);

namespace slapdash\core\library\widget;

use slapdash\core\library\widget\CoreWidgetItf;

abstract class CoreWidgetAbs implements CoreWidgetItf
{

	public function render(array $context):string
	{
		extract($context);
		ob_start();
		require $this->getViewFilePath();
		return ob_get_clean();
	}

		abstract protected function getViewFilePath():string;

}