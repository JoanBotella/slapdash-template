<?php
declare(strict_types=1);

namespace slapdash\core\library\controllerInputBuilder;

use slapdash\core\library\translation\CorePageTranslationItf;
use slapdash\core\service\languageContainer\CoreLanguageContainer;
use slapdash\core\service\requestPathContainer\CoreRequestPathContainer;

abstract class CoreControllerInputBuilderAbs
{

	private array $matches;

	protected function tryToSetupMatches(string $patternVariables):void
	{
		unset($this->matches);

		$pattern =
			'/^'
			.$this->getLanguageCode()
			.'\/'
			.$this->getPageSlug()
			.'\/'
			.$patternVariables
			.'?$/'
		;

		$result = preg_match(
			$pattern,
			CoreRequestPathContainer::get(),
			$matches
		);

		if ($result !== 1)
		{
			return;
		}

		$this->matches = $matches;
	}

		private function getLanguageCode():string
		{
			$language = CoreLanguageContainer::get();
			return $language->getCode();
		}

		private function getPageSlug():string
		{
			$translation = $this->getTranslation();
			return $translation->getPageSlug();
		}

			abstract protected function getTranslation():CorePageTranslationItf;

	protected function hasMatches():bool
	{
		return isset($this->matches);
	}

	protected function hasMatchesKey(string $key):bool
	{
		return isset($this->matches[$key]);
	}

	protected function getMatchesKeyAfterHas(string $key):string
	{
		return $this->matches[$key];
	}

}