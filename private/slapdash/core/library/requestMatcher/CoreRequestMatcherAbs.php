<?php
declare(strict_types=1);

namespace slapdash\core\library\requestMatcher;

use slapdash\core\library\requestMatcher\CoreRequestMatcherItf;
use slapdash\core\service\requestPathContainer\CoreRequestPathContainer;

abstract class CoreRequestMatcherAbs implements CoreRequestMatcherItf
{

	protected function getRequestPath():string
	{
		return CoreRequestPathContainer::get();
	}

	protected function pathIs(string $path):bool
	{
		return $this->getRequestPath() == $path;
	}

	protected function pathBeginsWith(string $path):bool
	{
		$requestPath = $this->getRequestPath();
		return
			$requestPath == $path
			|| strpos(
				$requestPath,
				$path.'/'
			) === 0;
	}

	protected function pathMatchesRegexp(string $regexp):bool
	{
		return preg_match(
			$regexp,
			$this->getRequestPath()
		) === 1;
	}

}