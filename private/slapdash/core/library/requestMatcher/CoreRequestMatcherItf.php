<?php
declare(strict_types=1);

namespace slapdash\core\library\requestMatcher;

interface CoreRequestMatcherItf
{

	public function matches():bool;

}