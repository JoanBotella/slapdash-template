<?php
declare(strict_types=1);

namespace slapdash\core\library\translation;

interface CorePageTranslationItf
{

	public function getPageSlug():string;

	public function getPageTitle():string;

}