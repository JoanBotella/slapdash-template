<?php
declare(strict_types=1);

namespace slapdash\core\library\languageGuesser;

use slapdash\core\library\language\CoreLanguageItf;
use slapdash\core\service\configurationContainer\CoreConfigurationContainer;
use slapdash\core\service\requestPathContainer\CoreRequestPathContainer;

final class CoreLanguageGuesser
{

	private string $languageCode;

	private CoreLanguageItf $language;

	public function run():CoreLanguageItf
	{
		$configuration = CoreConfigurationContainer::get();

		self::tryToSetupLanguageCodeFromRequestPath();

		if (!isset($this->languageCode))
		{
			return $configuration->getDefaultLanguage();
		}

		self::tryToSetupLanguage(
			$configuration->getEnabledLanguages()
		);

		unset($this->languageCode);

		if (isset($this->language))
		{
			$language = $this->language;
			unset($this->language);
			return $language;
		}

		return $configuration->getDefaultLanguage();
	}

		private function tryToSetupLanguageCodeFromRequestPath():void
		{
			$result = preg_match(
				'/^([a-z]{2})$|^([a-z]{2})\//',
				CoreRequestPathContainer::get(),
				$matches
			);

			if (
				$result === 0
				|| $result === false
			)
			{
				return;
			}

			$this->languageCode = 
				$matches[1] == ''
					? $matches[2]
					: $matches[1]
			;
		}

		private function tryToSetupLanguage(array $enabledLanguages):void
		{
			foreach ($enabledLanguages as $language)
			{
				if (self::isTheLanguageForTheLanguageCode($language))
				{
					$this->language = $language;
					return;
				}
			}
		}

			private function isTheLanguageForTheLanguageCode(CoreLanguageItf $language):bool
			{
				return $language->getCode() == $this->languageCode;
			}

}