<?php
declare(strict_types=1);

namespace slapdash\core\library\responder;

use slapdash\core\library\CoreHttpStatusConstant;
use slapdash\core\service\configurationContainer\CoreConfigurationContainer;
use slapdash\core\service\languageContainer\CoreLanguageContainer;
use slapdash\core\service\widget\html\CoreHtmlWidget;
use slapdash\core\service\widget\html\CoreHtmlWidgetContext;

abstract class CoreResponderAbs
{

	protected static function respond():void
	{
		http_response_code(
			static::getHttpStatus()
		);

		$context = new CoreHtmlWidgetContext(
			static::getLangCode(),
			static::getAppCode(),
			static::getPageCode(),
			static::getHeadTitle()
		);

		$context->setBodyContent(
			static::buildBodyContent()
		);

		$context->setScriptUrls(
			static::buildScriptUrls()
		);

		$context->setStyleUrls(
			static::buildStyleUrls()
		);

		echo CoreHtmlWidget::render($context);
	}

		protected static function getHttpStatus():int
		{
			return CoreHttpStatusConstant::OK;
		}

		protected static function getLangCode():string
		{
			$language = CoreLanguageContainer::get();
			return $language->getCode();
		}

		protected static function getAppCode():string
		{
			$configuration = CoreConfigurationContainer::get();
			return $configuration->getAppCode();
		}

		abstract protected static function getPageCode():string;

		protected static function getHeadTitle():string
		{
			$configuration = CoreConfigurationContainer::get();
			return
				static::getPageTitle()
				.' | '
				.$configuration->getAppName()
			;
		}
	
			abstract protected static function getPageTitle():string;
	
		abstract protected static function buildBodyContent():string;

		protected static function buildScriptUrls():array
		{
			return [];
		}

		protected static function buildStyleUrls():array
		{
			return [];
		}

}