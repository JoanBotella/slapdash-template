<?php
declare(strict_types=1);

namespace slapdash\core\library\language;

interface CoreLanguageItf
{

	public function getCode():string;

	public function getName():string;

}