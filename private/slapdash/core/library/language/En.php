<?php
declare(strict_types=1);

namespace slapdash\core\library\language;

use slapdash\core\library\language\CoreLanguageItf;

final class En implements CoreLanguageItf
{
	const CODE = 'en';

	public function getCode(): string
	{
		return self::CODE;
	}

	public function getName(): string
	{
		return 'English';
	}

}