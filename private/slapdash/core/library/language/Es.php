<?php
declare(strict_types=1);

namespace slapdash\core\library\language;

use slapdash\core\library\language\CoreLanguageItf;

final class Es implements CoreLanguageItf
{
	const CODE = 'es';

	public function getCode(): string
	{
		return self::CODE;
	}

	public function getName(): string
	{
		return 'castellano';
	}

}