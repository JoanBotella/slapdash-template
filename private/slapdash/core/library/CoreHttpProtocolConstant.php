<?php
declare(strict_types=1);

namespace slapdash\core\library;

final class CoreHttpProtocolConstant
{

	const HTTP = 'http';

	const HTTPS = 'https';

}