<?php
declare(strict_types=1);

namespace slapdash\core\library\configuration;

use slapdash\core\library\language\CoreLanguageItf;

interface CoreConfigurationItf
{

	public static function getAppName():string;

	public static function getAppDirectoryName():string;

	public static function getAppCode():string;

	public static function getAppUrlProtocol():string;

	public static function getAppUrlHost():string;

	public static function getAppUrlBasePath():string;

	public static function getRoutes():array;

	public static function getEnabledLanguages():array;

	public static function getDefaultLanguage():CoreLanguageItf;

}