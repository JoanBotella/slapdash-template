<?php
declare(strict_types=1);

namespace slapdash\core\library\configuration;

use slapdash\core\library\configuration\CoreConfigurationItf;
use slapdash\core\library\language\Ca;
use slapdash\core\library\language\En;
use slapdash\core\library\language\Es;
use slapdash\core\library\language\CoreLanguageItf;

abstract class CoreConfigurationAbs implements CoreConfigurationItf
{

	public static function getEnabledLanguages():array
	{
		return [
			new En(),
			new Es(),
			new Ca(),
		];
	}

	public static function getDefaultLanguage():CoreLanguageItf
	{
		return new En();
	}

}