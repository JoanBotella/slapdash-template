<?php
declare(strict_types=1);

namespace slapdash\core\library\route;

use slapdash\core\library\controller\CoreControllerItf;
use slapdash\core\library\requestMatcher\CoreRequestMatcherItf;

interface CoreRouteItf
{

	public function buildController():CoreControllerItf;

	public function buildRequestMatcher():CoreRequestMatcherItf;

}