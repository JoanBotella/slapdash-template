<?php
declare(strict_types=1);

namespace slapdash\core\library\urlBuilder;

use slapdash\core\library\language\CoreLanguageItf;
use slapdash\core\service\configurationContainer\CoreConfigurationContainer;
use slapdash\core\service\languageContainer\CoreLanguageContainer;

abstract class CoreUrlBuilderAbs
{

	protected static function buildAppUrlProtocolAndHost():string
	{
		$configuration = CoreConfigurationContainer::get();
		return
			$configuration->getAppUrlProtocol()
			.'//'
			.$configuration->getAppUrlHost()
		;
	}

	protected static function buildAppUrlBasePathAndLanguageSegment(CoreLanguageItf $language):string
	{
		$configuration = CoreConfigurationContainer::get();
		return
			$configuration->getAppUrlBasePath()
			.'/'
			.$language->getCode()
		;
	}

}
