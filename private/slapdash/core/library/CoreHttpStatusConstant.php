<?php
declare(strict_types=1);

namespace slapdash\core\library;

final class CoreHttpStatusConstant
{

	const OK = 200;

	const BAD_REQUEST = 400;

	const NOT_FOUND = 404;

}