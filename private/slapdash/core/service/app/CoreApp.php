<?php
declare(strict_types=1);

namespace slapdash\core\service\app;

use slapdash\core\service\router\CoreRouter;

final class CoreApp
{

	public static function run():void
	{
		CoreRouter::route();
	}

}