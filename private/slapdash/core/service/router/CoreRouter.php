<?php
declare(strict_types=1);

namespace slapdash\core\service\router;

use Exception;
use slapdash\core\library\route\CoreRouteItf;
use slapdash\core\service\configurationContainer\CoreConfigurationContainer;

final class CoreRouter
{

	private static bool $hasMatched;

	public static function route():void
	{
		self::$hasMatched = false;

		$routes = self::getRoutes();

		foreach ($routes as $route)
		{
			self::runRouteControllerIfMatches($route);

			if (self::$hasMatched)
			{
				return;
			}
		}

		throw new Exception('Route not found');
	}

		private static function getRoutes():array
		{
			$configuration = CoreConfigurationContainer::get();
			return $configuration::getRoutes();
		}

		private static function runRouteControllerIfMatches(CoreRouteItf $route):void
		{
			$requestMatcher = $route->buildRequestMatcher();
			if ($requestMatcher->matches())
			{
				self::$hasMatched = true;

				$controller = $route->buildController();
				$controller->action();
			}
		}

}