<?php
declare(strict_types=1);

namespace slapdash\core\service\serverSuperglobalWrapper;

final class CoreServerSuperglobalWrapper
{

	public static function getRequestUri():string
	{
		return $_SERVER['REQUEST_URI'];
	}

}
