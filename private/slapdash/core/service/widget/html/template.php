<?php
declare(strict_types=1);

use slapdash\core\service\widget\html\CoreHtmlWidgetContext;

/**
 * @var CoreHtmlWidgetContext $context
 */

?><!DOCTYPE html>
<html lang="<?= $context->getLangCode() ?>" data-app="<?= $context->getAppCode() ?>" data-page="<?= $context->getPageCode() ?>" class="app-<?= $context->getAppCode() ?> page-<?= $context->getPageCode() ?>">
<head>
	<meta charset="UTF-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

	<?php if ($context->hasFaviconUrl()): ?>
	<link rel="icon" href="<?= $context->getFaviconUrlAfterHas() ?>" type="image/png" />
	<?php endif; ?>

	<title><?= $context->getHeadTitle() ?></title>

	<?php
		if ($context->hasStyleUrls()):
			foreach ($context->getStyleUrlsAfterHas() as $url):
	?>
	<link href="<?= $url ?>" rel="stylesheet" media="screen" />
	<?php
			endforeach;
		endif;
	?>

</head>
<body>

	<?php if ($context->hasBodyContent()): ?>
	<?= $context->getBodyContentAfterHas() ?>
	<?php endif; ?>

	<?php
		if ($context->hasScriptUrls()):
			foreach ($context->getScriptUrlsAfterHas() as $url):
	?>
	<script src="<?= $url ?>"></script>
	<?php
			endforeach;
		endif;
	?>

</body>
</html>