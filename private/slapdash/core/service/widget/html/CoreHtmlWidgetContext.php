<?php
declare(strict_types=1);

namespace slapdash\core\service\widget\html;

final class CoreHtmlWidgetContext
{

	private string $langCode;

	private string $headTitle;

	private string $appCode;

	private string $pageCode;

	private string $bodyContent;

	private string $faviconUrl;

	private array $styleUrls;

	private array $scriptUrls;

	public function __construct(
		string $langCode,
		string $appCode,
		string $pageCode,
		string $headTitle
	)
	{
		$this->langCode = $langCode;
		$this->appCode = $appCode;
		$this->pageCode = $pageCode;
		$this->headTitle = $headTitle;
	}

	public function getLangCode():string
	{
		return $this->langCode;
	}

	public function getAppCode():string
	{
		return $this->appCode;
	}

	public function getPageCode():string
	{
		return $this->pageCode;
	}

	public function getHeadTitle():string
	{
		return $this->headTitle;
	}

	public function hasBodyContent():bool
	{
		return isset(
			$this->bodyContent
		);
	}

	public function getBodyContentAfterHas():string
	{
		return $this->bodyContent;
	}

	public function setBodyContent(string $v):void
	{
		$this->bodyContent = $v;
	}

	public function unsetBodyContent():void
	{
		unset(
			$this->bodyContent
		);
	}

	public function hasFaviconUrl():bool
	{
		return isset(
			$this->faviconUrl
		);
	}

	public function getFaviconUrlAfterHas():string
	{
		return $this->faviconUrl;
	}

	public function setFaviconUrl(string $v):void
	{
		$this->faviconUrl = $v;
	}

	public function unsetFaviconUrl():void
	{
		unset(
			$this->faviconUrl
		);
	}

	public function hasStyleUrls():bool
	{
		return isset(
			$this->styleUrls
		);
	}

	public function getStyleUrlsAfterHas():array
	{
		return $this->styleUrls;
	}

	public function setStyleUrls(array $v):void
	{
		$this->styleUrls = $v;
	}

	public function unsetStyleUrls():void
	{
		unset(
			$this->styleUrls
		);
	}

	public function hasScriptUrls():bool
	{
		return isset(
			$this->scriptUrls
		);
	}

	public function getScriptUrlsAfterHas():array
	{
		return $this->scriptUrls;
	}

	public function setScriptUrls(array $v):void
	{
		$this->scriptUrls = $v;
	}

	public function unsetScriptUrls():void
	{
		unset(
			$this->scriptUrls
		);
	}

}