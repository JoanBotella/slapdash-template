<?php
declare(strict_types=1);

namespace slapdash\core\service\widget\html;

use slapdash\core\service\widget\html\CoreHtmlWidgetContext;

final class CoreHtmlWidget
{

	public static function render(CoreHtmlWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/template.php';
		return ob_get_clean();
	}

}