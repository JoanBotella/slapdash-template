<?php
declare(strict_types=1);

namespace slapdash\core\service\languageContainer;

use slapdash\core\library\language\CoreLanguageItf;
use slapdash\core\library\languageGuesser\CoreLanguageGuesser;

final class CoreLanguageContainer
{

	private static CoreLanguageItf $language;

	public static function get():CoreLanguageItf
	{
		if (!isset(self::$language))
		{
			self::$language = self::buildLanguage();
		}
		return self::$language;
	}

		private static function buildLanguage():CoreLanguageItf
		{
			$languageGuesser = new CoreLanguageGuesser();
			return $languageGuesser->run();
		}

}
