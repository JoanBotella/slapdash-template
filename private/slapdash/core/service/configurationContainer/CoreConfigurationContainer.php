<?php
declare(strict_types=1);

namespace slapdash\core\service\configurationContainer;

use slapdash\core\library\configuration\CoreConfigurationItf;

final class CoreConfigurationContainer
{

	private static CoreConfigurationItf $configuration;

	public static function get():CoreConfigurationItf
	{
		return self::$configuration;
	}

	public static function set(CoreConfigurationItf $v):void
	{
		self::$configuration = $v;
	}

}