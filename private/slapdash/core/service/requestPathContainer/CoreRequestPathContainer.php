<?php
declare(strict_types=1);

namespace slapdash\core\service\requestPathContainer;

use slapdash\core\service\configurationContainer\CoreConfigurationContainer;
use slapdash\core\service\serverSuperglobalWrapper\CoreServerSuperglobalWrapper;

final class CoreRequestPathContainer
{

	private static string $path;

	public static function get():string
	{
		if (!isset(self::$path))
		{
			self::$path = self::buildPath();
		}
		return self::$path;
	}

		private static function buildPath():string
		{
			$configuration = CoreConfigurationContainer::get();
			return str_replace(
				'/'.$configuration->getAppUrlBasePath().'/',
				'',
				CoreServerSuperglobalWrapper::getRequestUri()
			);
		}

}