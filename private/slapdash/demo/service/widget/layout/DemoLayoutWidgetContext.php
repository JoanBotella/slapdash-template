<?php
declare(strict_types=1);

namespace slapdash\demo\service\widget\layout;

use slapdash\demo\library\translation\DemoTranslationItf;

final class DemoLayoutWidgetContext
{

	private DemoTranslationItf $translation;

	private string $homeUrl;

	private string $aboutUrl;

	private string $greetZeldaUrl;

	private string $greetHoffmanUrl;

	private string $mainContent;

	private array $translationAnchorElemens;

	public function __construct(
		DemoTranslationItf $translation,
		string $homeUrl,
		string $aboutUrl,
		string $greetZeldaUrl,
		string $greetHoffmanUrl,
		array $translationAnchorElemens
	)
	{
		$this->translation = $translation;
		$this->homeUrl = $homeUrl;
		$this->aboutUrl = $aboutUrl;
		$this->greetZeldaUrl = $greetZeldaUrl;
		$this->greetHoffmanUrl = $greetHoffmanUrl;
		$this->translationAnchorElemens = $translationAnchorElemens;
	}

	public function getTranslation():DemoTranslationItf
	{
		return $this->translation;
	}

	public function getHomeUrl():string
	{
		return $this->homeUrl;
	}

	public function getAboutUrl():string
	{
		return $this->aboutUrl;
	}

	public function getGreetZeldaUrl():string
	{
		return $this->greetZeldaUrl;
	}

	public function getGreetHoffmanUrl():string
	{
		return $this->greetHoffmanUrl;
	}

	public function getTranslationAnchorElements():array
	{
		return $this->translationAnchorElemens;
	}

	public function hasMainContent():bool
	{
		return isset(
			$this->mainContent
		);
	}

	public function getMainContentAfterHas():string
	{
		return $this->mainContent;
	}

	public function setMainContent(string $v):void
	{
		$this->mainContent = $v;
	}

	public function unsetMainContent():void
	{
		unset(
			$this->mainContent
		);
	}

}