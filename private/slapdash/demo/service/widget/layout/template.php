<?php
declare(strict_types=1);

use slapdash\demo\service\widget\layout\DemoLayoutWidgetContext;

/**
 * @var DemoLayoutWidgetContext $context
 */

?>

<div data-widget="demo-layout" class="demo-layout">

	<header></header>

	<nav>
		<ul>
			<li><a href="<?= $context->getHomeUrl() ?>"><?= $context->getTranslation()->getNavHomeLinkContent() ?></a></li>
			<li><a href="<?= $context->getAboutUrl() ?>"><?= $context->getTranslation()->getNavAboutLinkContent() ?></a></li>
			<li><a href="<?= $context->getGreetZeldaUrl() ?>"><?= $context->getTranslation()->getNavGreetZeldaLinkContent() ?></a></li>
			<li><a href="<?= $context->getGreetHoffmanUrl() ?>"><?= $context->getTranslation()->getNavGreetHoffmanLinkContent() ?></a></li>
		</ul>
	</nav>

	<main>
<?= $context->getMainContentAfterHas() ?>


	</main>

	<footer>
		<nav>
			<ul>
				<?php foreach($context->getTranslationAnchorElements() as $translationAnchorElement): ?>
					<li><?= $translationAnchorElement ?></li>
				<?php endforeach ?>
			</ul>
		</nav>
	</footer>

</div>