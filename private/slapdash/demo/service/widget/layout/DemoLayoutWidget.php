<?php
declare(strict_types=1);

namespace slapdash\demo\service\widget\layout;

use slapdash\demo\service\widget\layout\DemoLayoutWidgetContext;

final class DemoLayoutWidget
{

	public static function render(DemoLayoutWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/template.php';
		return ob_get_clean();
	}

}