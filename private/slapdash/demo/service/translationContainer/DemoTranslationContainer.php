<?php
declare(strict_types=1);

namespace slapdash\demo\service\translationContainer;

use slapdash\demo\library\translation\DemoTranslationItf;
use slapdash\demo\service\translationBuilder\DemoTranslationBuilder;
use slapdash\core\library\language\CoreLanguageItf;
use slapdash\core\service\languageContainer\CoreLanguageContainer;

final class DemoTranslationContainer
{

	private static array $translations;

	public static function get():DemoTranslationItf
	{
		return self::getByLanguage(
			CoreLanguageContainer::get()
		);
	}

	public static function getByLanguage(CoreLanguageItf $language):DemoTranslationItf
	{
		$languageCode = $language->getCode();
		if (!isset(self::$translations[$languageCode]))
		{
			self::$translations[$languageCode] = self::buildTranslation($language);
		}
		return self::$translations[$languageCode];
	}

		private static function buildTranslation(CoreLanguageItf $language):DemoTranslationItf
		{
			return DemoTranslationBuilder::build(
				$language
			);
		}

}