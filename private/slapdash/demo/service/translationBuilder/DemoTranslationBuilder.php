<?php
declare(strict_types=1);

namespace slapdash\demo\service\translationBuilder;

use slapdash\demo\library\translation\DemoTranslationItf;
use slapdash\core\library\language\CoreLanguageItf;
use Exception;
use slapdash\demo\library\translation\ca\DemoCaTranslation;
use slapdash\demo\library\translation\en\DemoEnTranslation;
use slapdash\demo\library\translation\es\DemoEsTranslation;
use slapdash\core\library\language\Ca;
use slapdash\core\library\language\En;
use slapdash\core\library\language\Es;

final class DemoTranslationBuilder
{

	public static function build(CoreLanguageItf $language):DemoTranslationItf
	{
		switch ($language->getCode())
		{
			case En::CODE:
				return new DemoEnTranslation();
			case Es::CODE:
				return new DemoEsTranslation();
			case Ca::CODE:
				return new DemoCaTranslation();
		}
		throw new Exception('The language code '.$language->getCode().' is not valid.');
	}

}