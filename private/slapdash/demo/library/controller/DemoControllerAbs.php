<?php
declare(strict_types=1);

namespace slapdash\demo\library\controller;

use slapdash\demo\page\badRequest\library\controller\DemoBadRequestController;
use slapdash\demo\page\notFound\library\controller\DemoNotFoundController;
use slapdash\core\library\controller\CoreControllerItf;

abstract class DemoControllerAbs implements CoreControllerItf
{

	protected function runBadRequestControllerAction():void
	{
		$controller = new DemoBadRequestController();
		$controller->action();
	}

	protected function runNotFoundControllerAction():void
	{
		$controller = new DemoNotFoundController();
		$controller->action();
	}

}