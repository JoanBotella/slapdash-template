<?php
declare(strict_types=1);

namespace slapdash\demo\library\responder;

use slapdash\demo\page\about\service\urlBuilder\DemoAboutUrlBuilder;
use slapdash\demo\page\greet\service\urlBuilder\DemoGreetUrlBuilder;
use slapdash\demo\page\home\service\urlBuilder\DemoHomeUrlBuilder;
use slapdash\demo\service\translationContainer\DemoTranslationContainer;
use slapdash\demo\service\widget\layout\DemoLayoutWidget;
use slapdash\demo\service\widget\layout\DemoLayoutWidgetContext;
use slapdash\core\library\language\CoreLanguageItf;
use slapdash\core\library\responder\CoreResponderAbs;
use slapdash\core\service\configurationContainer\CoreConfigurationContainer;

abstract class DemoResponderAbs extends CoreResponderAbs
{

	protected static function buildBodyContent():string
	{
		$context = new DemoLayoutWidgetContext(
			DemoTranslationContainer::get(),
			'/'.DemoHomeUrlBuilder::buildPath(),
			'/'.DemoAboutUrlBuilder::buildPath(),
			'/'.DemoGreetUrlBuilder::buildPathByNameAndTimes('Zelda', 3),
			'/'.DemoGreetUrlBuilder::buildPathByName('Hoffman'),
			static::buildTranslationAnchorElements()
		);

		$context->setMainContent(
			static::buildMainContent()
		);

		return DemoLayoutWidget::render($context);
	}

		abstract protected static function buildTranslationAnchorElements():array;

		abstract protected static function buildMainContent():string;

		protected static function buildScriptUrls(): array
		{
			$configuration = CoreConfigurationContainer::get();
			return array_merge(
				parent::buildScriptUrls(),
				[
					'/'.$configuration->getAppUrlBasePath().'/'.$configuration->getAppDirectoryName().'/bundle/demo/asset/js/script.js',
				]
			);
		}
	
		protected static function buildStyleUrls(): array
		{
			$configuration = CoreConfigurationContainer::get();
			return array_merge(
				parent::buildStyleUrls(),
				[
					'/'.$configuration->getAppUrlBasePath().'/'.$configuration->getAppDirectoryName().'/bundle/demo/asset/css/style.css',
				]
			);
		}

	protected static function getEnabledLanguages():array
	{
		$configuration = CoreConfigurationContainer::get();
		return $configuration->getEnabledLanguages();
	}

	protected static function addTranslationAnchorElement(array $translationAnchorElements, CoreLanguageItf $language, string $url):array
	{
		// !!! Create a XmlElementWidget and then an HtmlAnchorElementWidget
		$translationAnchorElements[] = '<a href="/'.$url.'" hreflang="'.$language->getCode().'">'.$language->getName().'</a>';
		return $translationAnchorElements;
	}

}