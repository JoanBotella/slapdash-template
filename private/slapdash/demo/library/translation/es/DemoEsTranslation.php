<?php
declare(strict_types=1);

namespace slapdash\demo\library\translation\es;

use slapdash\demo\library\translation\DemoTranslationItf;

final class DemoEsTranslation implements DemoTranslationItf
{

	public function getNavAboutLinkContent(): string
	{
		return 'Sobre';
	}

	public function getNavHomeLinkContent(): string
	{
		return 'Inicio';
	}

	public function getNavGreetZeldaLinkContent(): string
	{
		return 'Saluda a Zelda';
	}

	public function getNavGreetHoffmanLinkContent(): string
	{
		return 'Saluda a Hoffman';
	}

}
