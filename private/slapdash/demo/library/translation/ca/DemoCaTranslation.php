<?php
declare(strict_types=1);

namespace slapdash\demo\library\translation\ca;

use slapdash\demo\library\translation\DemoTranslationItf;

final class DemoCaTranslation implements DemoTranslationItf
{

	public function getNavAboutLinkContent(): string
	{
		return 'Sobre';
	}

	public function getNavHomeLinkContent(): string
	{
		return 'Inici';
	}

	public function getNavGreetZeldaLinkContent(): string
	{
		return 'Saluda a Zelda';
	}

	public function getNavGreetHoffmanLinkContent(): string
	{
		return 'Saluda a Hoffman';
	}

}
