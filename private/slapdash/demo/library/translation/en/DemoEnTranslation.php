<?php
declare(strict_types=1);

namespace slapdash\demo\library\translation\en;

use slapdash\demo\library\translation\DemoTranslationItf;

final class DemoEnTranslation implements DemoTranslationItf
{

	public function getNavAboutLinkContent(): string
	{
		return 'About';
	}

	public function getNavHomeLinkContent(): string
	{
		return 'Home';
	}

	public function getNavGreetZeldaLinkContent(): string
	{
		return 'Greet Zelda';
	}

	public function getNavGreetHoffmanLinkContent(): string
	{
		return 'Greet Hoffman';
	}

}
