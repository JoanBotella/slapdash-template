<?php
declare(strict_types=1);

namespace slapdash\demo\library\translation;

interface DemoTranslationItf
{

	public function getNavAboutLinkContent():string;

	public function getNavHomeLinkContent():string;

	public function getNavGreetZeldaLinkContent():string;

	public function getNavGreetHoffmanLinkContent():string;

}