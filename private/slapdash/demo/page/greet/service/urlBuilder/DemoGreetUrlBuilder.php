<?php
declare(strict_types=1);

namespace slapdash\demo\page\greet\service\urlBuilder;

use slapdash\demo\page\greet\service\translationContainer\DemoGreetTranslationContainer;
use slapdash\core\library\language\CoreLanguageItf;
use slapdash\core\library\urlBuilder\CoreUrlBuilderAbs;
use slapdash\core\service\languageContainer\CoreLanguageContainer;

final class DemoGreetUrlBuilder extends CoreUrlBuilderAbs
{

	public static function buildAbsoluteByName(string $name):string
	{
		return self::buildAbsoluteByLanguageAndName(
			CoreLanguageContainer::get(),
			$name
		);
	}

	public static function buildAbsoluteByLanguageAndName(CoreLanguageItf $language, string $name):string
	{
		return
			self::buildAppUrlProtocolAndHost()
			.'/'
			.self::buildPathByLanguageAndName($language, $name)
		;
	}

	public static function buildPathByName(string $name):string
	{
		return self::buildPathByLanguageAndName(
			CoreLanguageContainer::get(),
			$name
		);
	}

	public static function buildPathByLanguageAndName(CoreLanguageItf $language, string $name):string
	{
		$translation = DemoGreetTranslationContainer::getByLanguage($language);
		return
			self::buildAppUrlBasePathAndLanguageSegment($language)
			.'/'
			.$translation->getPageSlug()
			.'/'
			.$name
		;
	}

	public static function buildAbsoluteByNameAndTimes(string $name, int $times):string
	{
		return self::buildAbsoluteByLanguageAndNameAndTimes(
			CoreLanguageContainer::get(),
			$name,
			$times
		);
	}

	public static function buildAbsoluteByLanguageAndNameAndTimes(CoreLanguageItf $language, string $name, int $times):string
	{
		return
			self::buildAppUrlProtocolAndHost()
			.'/'
			.self::buildPathByLanguageAndNameAndTimes($language, $name, $times)
		;
	}

	public static function buildPathByNameAndTimes(string $name, int $times):string
	{
		return self::buildPathByLanguageAndNameAndTimes(
			CoreLanguageContainer::get(),
			$name,
			$times
		);
	}

	public static function buildPathByLanguageAndNameAndTimes(CoreLanguageItf $language, string $name, int $times):string
	{
		$translation = DemoGreetTranslationContainer::getByLanguage($language);
		return
			self::buildAppUrlBasePathAndLanguageSegment($language)
			.'/'
			.$translation->getPageSlug()
			.'/'
			.$name
			.'/'
			.$times
		;
	}

}