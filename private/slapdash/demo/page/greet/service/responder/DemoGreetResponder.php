<?php
declare(strict_types=1);

namespace slapdash\demo\page\greet\service\responder;

use slapdash\demo\library\responder\DemoResponderAbs;
use slapdash\demo\page\greet\library\translation\DemoGreetTranslationItf;
use slapdash\demo\page\greet\service\translationContainer\DemoGreetTranslationContainer;
use slapdash\demo\page\greet\service\urlBuilder\DemoGreetUrlBuilder;
use slapdash\demo\page\greet\service\widget\content\DemoGreetContentWidget;
use slapdash\demo\page\greet\service\widget\content\DemoGreetContentWidgetContext;
use slapdash\core\library\language\CoreLanguageItf;

final class DemoGreetResponder extends DemoResponderAbs
{

	private static DemoGreetResponderInput $input;

	public static function run(DemoGreetResponderInput $input):void
	{
		self::$input = $input;
		static::respond();
	}

	protected static function getPageCode():string
	{
		return 'demo-greet';
	}

	protected static function buildTranslationAnchorElements(): array
	{
		$translationAnchorElements = [];

		foreach (static::getEnabledLanguages() as $language)
		{
			$url = static::buildUrlByLanguage($language);

			$translationAnchorElements = static::addTranslationAnchorElement(
				$translationAnchorElements,
				$language,
				$url
			);
		}

		return $translationAnchorElements;
	}

		private static function buildUrlByLanguage(CoreLanguageItf $language):string
		{
			return
				self::$input->hasTimes()
					?
						DemoGreetUrlBuilder::buildPathByLanguageAndNameAndTimes(
							$language,
							self::$input->getName(),
							self::$input->getTimesAfterHas()
						)
					:
						DemoGreetUrlBuilder::buildPathByLanguageAndName(
							$language,
							self::$input->getName()
						)
			;
	}

	protected static function buildMainContent():string
	{
		$times =
			self::$input->hasTimes()
				? self::$input->getTimesAfterHas()
				: 1
		;

		$context = new DemoGreetContentWidgetContext(
			self::getTranslation(),
			self::$input->getName(),
			$times
		);

		return DemoGreetContentWidget::render($context);
	}

		private static function getTranslation():DemoGreetTranslationItf
		{
			return DemoGreetTranslationContainer::get();
		}

	protected static function getPageTitle(): string
	{
		$translation = self::getTranslation();
		return $translation->getPageTitle();
	}


}