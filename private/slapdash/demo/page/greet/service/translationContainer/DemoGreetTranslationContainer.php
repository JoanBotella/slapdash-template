<?php
declare(strict_types=1);

namespace slapdash\demo\page\greet\service\translationContainer;

use slapdash\demo\page\greet\library\translation\DemoGreetTranslationItf;
use slapdash\demo\page\greet\service\translationBuilder\DemoGreetTranslationBuilder;
use slapdash\core\library\language\CoreLanguageItf;
use slapdash\core\service\languageContainer\CoreLanguageContainer;

final class DemoGreetTranslationContainer
{

	private static array $translations;

	public static function get():DemoGreetTranslationItf
	{
		return self::getByLanguage(
			CoreLanguageContainer::get()
		);
	}

	public static function getByLanguage(CoreLanguageItf $language):DemoGreetTranslationItf
	{
		$languageCode = $language->getCode();
		if (!isset(self::$translations[$languageCode]))
		{
			self::$translations[$languageCode] = self::buildTranslation($language);
		}
		return self::$translations[$languageCode];
	}

		private static function buildTranslation(CoreLanguageItf $language):DemoGreetTranslationItf
		{
			return DemoGreetTranslationBuilder::build(
				$language
			);
		}

}