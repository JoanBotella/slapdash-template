<?php
declare(strict_types=1);

namespace slapdash\demo\page\greet\service\widget\content;

use slapdash\demo\page\greet\service\widget\content\DemoGreetContentWidgetContext;

final class DemoGreetContentWidget
{

	public static function render(DemoGreetContentWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/template.php';
		return ob_get_clean();
	}

}