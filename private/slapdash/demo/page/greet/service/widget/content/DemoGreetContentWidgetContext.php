<?php
declare(strict_types=1);

namespace slapdash\demo\page\greet\service\widget\content;

use slapdash\demo\page\greet\library\translation\DemoGreetTranslationItf;

final class DemoGreetContentWidgetContext
{

	private DemoGreetTranslationItf $translation;

	private string $name;

	private int $times;

	public function __construct(
		DemoGreetTranslationItf $translation,
		string $name,
		int $times
	)
	{
		$this->translation = $translation;
		$this->name = $name;
		$this->times = $times;
	}

	public function getTranslation():DemoGreetTranslationItf
	{
		return $this->translation;
	}

	public function getName():string
	{
		return $this->name;
	}

	public function getTimes():int
	{
		return $this->times;
	}

}