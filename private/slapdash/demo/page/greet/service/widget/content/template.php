<?php
declare(strict_types=1);

use slapdash\demo\page\greet\service\widget\content\DemoGreetContentWidgetContext;

/**
 * @var DemoGreetContentWidgetContext $context
 */

?>

<h1><?= $context->getTranslation()->getPageTitle() ?></h1>

<ul>

	<?php for($i = 0; $i < $context->getTimes(); $i++): ?>
	<li><?= $context->getTranslation()->content_greet($context->getName()) ?></li>
	<?php endfor ?>

</ul>