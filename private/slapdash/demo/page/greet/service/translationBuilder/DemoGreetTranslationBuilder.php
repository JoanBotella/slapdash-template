<?php
declare(strict_types=1);

namespace slapdash\demo\page\greet\service\translationBuilder;

use slapdash\demo\page\greet\library\translation\DemoGreetTranslationItf;
use slapdash\core\library\language\CoreLanguageItf;
use Exception;
use slapdash\demo\page\greet\library\translation\ca\DemoGreetCaTranslation;
use slapdash\demo\page\greet\library\translation\en\DemoGreetEnTranslation;
use slapdash\demo\page\greet\library\translation\es\DemoGreetEsTranslation;
use slapdash\core\library\language\Ca;
use slapdash\core\library\language\En;
use slapdash\core\library\language\Es;

final class DemoGreetTranslationBuilder
{

	public static function build(CoreLanguageItf $language):DemoGreetTranslationItf
	{
		switch ($language->getCode())
		{
			case En::CODE:
				return new DemoGreetEnTranslation();
			case Es::CODE:
				return new DemoGreetEsTranslation();
			case Ca::CODE:
				return new DemoGreetCaTranslation();
		}
		throw new Exception('The language code '.$language->getCode().' is not valid.');
	}

}