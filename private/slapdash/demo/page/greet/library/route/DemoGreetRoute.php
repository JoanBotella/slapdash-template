<?php
declare(strict_types=1);

namespace slapdash\demo\page\greet\library\route;

use slapdash\demo\page\greet\library\controller\DemoGreetController;
use slapdash\demo\page\greet\library\requestMatcher\DemoGreetRequestMatcher;
use slapdash\core\library\controller\CoreControllerItf;
use slapdash\core\library\requestMatcher\CoreRequestMatcherItf;
use slapdash\core\library\route\CoreRouteItf;

final class DemoGreetRoute implements CoreRouteItf
{

	public function buildController():CoreControllerItf
	{
		return new DemoGreetController();
	}

	public function buildRequestMatcher():CoreRequestMatcherItf
	{
		return new DemoGreetRequestMatcher();
	}

}