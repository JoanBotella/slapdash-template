<?php
declare(strict_types=1);

namespace slapdash\demo\page\greet\library\translation;

use slapdash\core\library\translation\CorePageTranslationItf;

interface DemoGreetTranslationItf extends CorePageTranslationItf
{

	public function content_greet(string $name):string;

}