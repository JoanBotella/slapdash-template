<?php
declare(strict_types=1);

namespace slapdash\demo\page\greet\library\translation\es;

use slapdash\demo\page\greet\library\translation\DemoGreetTranslationItf;

final class DemoGreetEsTranslation implements DemoGreetTranslationItf
{

	public function getPageSlug():string { return 'saluda'; }

	public function getPageTitle():string { return 'Saluda'; }

	public function content_greet(string $name):string { return 'Hola '.$name; }

}