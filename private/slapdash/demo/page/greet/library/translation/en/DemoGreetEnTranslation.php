<?php
declare(strict_types=1);

namespace slapdash\demo\page\greet\library\translation\en;

use slapdash\demo\page\greet\library\translation\DemoGreetTranslationItf;

final class DemoGreetEnTranslation implements DemoGreetTranslationItf
{

	public function getPageSlug():string { return 'greet'; }

	public function getPageTitle():string { return 'Greet'; }

	public function content_greet(string $name):string { return 'Hello '.$name; }

}