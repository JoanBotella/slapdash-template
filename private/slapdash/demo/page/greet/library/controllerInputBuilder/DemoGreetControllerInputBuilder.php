<?php
declare(strict_types=1);

namespace slapdash\demo\page\greet\library\controllerInputBuilder;

use slapdash\demo\page\greet\library\controller\DemoGreetControllerInput;
use slapdash\demo\page\greet\service\translationContainer\DemoGreetTranslationContainer;
use slapdash\core\library\controllerInputBuilder\CoreControllerInputBuilderAbs;
use slapdash\core\library\translation\CorePageTranslationItf;

final class DemoGreetControllerInputBuilder extends CoreControllerInputBuilderAbs
{
	const KEY_NAME = 'name';
	const KEY_TIMES = 'times';

	private DemoGreetControllerInput $built;

	public function build():void
	{
		unset($this->input);

		$this->tryToSetupMatches(
			'(?P<'.self::KEY_NAME.'>\w+)(\/(?P<'.self::KEY_TIMES.'>\d+))'
		);

		if (!$this->hasMatches())
		{
			return;
		}

		if (!$this->hasMatchesKey(self::KEY_NAME))
		{
			return;
		}

		$input = new DemoGreetControllerInput(
			$this->getMatchesKeyAfterHas(self::KEY_NAME)
		);

		if ($this->hasMatchesKey(self::KEY_TIMES))
		{
			$input->setTimes(
				(int) $this->getMatchesKeyAfterHas(self::KEY_TIMES)
			);
		}

		$this->built = $input;
	}

	public function hasBuilt():bool
	{
		return isset($this->built);
	}

	public function getBuiltAfterHas():DemoGreetControllerInput
	{
		return $this->built;
	}

	protected function getTranslation():CorePageTranslationItf
	{
		return DemoGreetTranslationContainer::get();
	}

}