<?php
declare(strict_types=1);

namespace slapdash\demo\page\greet\library\controller;

final class DemoGreetControllerInput
{

	private string $name;

	private int $times;

	public function __construct(
		string $name
	)
	{
		$this->name = $name;
	}

	public function getName():string
	{
		return $this->name;
	}

	public function hasTimes():bool
	{
		return isset(
			$this->times
		);
	}

	public function getTimesAfterHas():int
	{
		return $this->times;
	}

	public function setTimes(int $v):void
	{
		$this->times = $v;
	}

	public function unsetTimes():void
	{
		unset(
			$this->times
		);
	}

}