<?php
declare(strict_types=1);

namespace slapdash\demo\page\greet\library\controller;

use slapdash\demo\library\controller\DemoControllerAbs;
use slapdash\demo\page\greet\library\controllerInputBuilder\DemoGreetControllerInputBuilder;
use slapdash\demo\page\greet\service\responder\DemoGreetResponder;
use slapdash\demo\page\greet\service\responder\DemoGreetResponderInput;

final class DemoGreetController extends DemoControllerAbs
{

	private DemoGreetControllerInput $input;

	public function action():void
	{
		$this->tryToSetupInput();

		if (!isset($this->input))
		{
			$this->runBadRequestControllerAction();
			return;
		}

		DemoGreetResponder::run(
			$this->buildResponderInput()
		);
	}

		private function tryToSetupInput():void
		{
			$inputBuilder = new DemoGreetControllerInputBuilder();
			$inputBuilder->build();
			if ($inputBuilder->hasBuilt())
			{
				$this->input = $inputBuilder->getBuiltAfterHas();
			}
		}

		private function buildResponderInput():DemoGreetResponderInput
		{
			$responderInput = new DemoGreetResponderInput(
				$this->input->getName()
			);

			if ($this->input->hasTimes())
			{
				$responderInput->setTimes(
					$this->input->getTimesAfterHas()
				);
			}

			return $responderInput;
		}

}
