<?php
declare(strict_types=1);

namespace slapdash\demo\page\greet\library\requestMatcher;

use slapdash\demo\page\greet\service\translationContainer\DemoGreetTranslationContainer;
use slapdash\core\library\requestMatcher\CoreRequestMatcherAbs;
use slapdash\core\service\languageContainer\CoreLanguageContainer;

final class DemoGreetRequestMatcher extends CoreRequestMatcherAbs
{

	public function matches():bool
	{
		$translation = DemoGreetTranslationContainer::get();
		$language = CoreLanguageContainer::get();
		return $this->pathBeginsWith(
			$language->getCode().'/'.$translation->getPageSlug()
		);
	}

}