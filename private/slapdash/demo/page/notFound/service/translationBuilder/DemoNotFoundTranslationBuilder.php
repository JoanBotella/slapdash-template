<?php
declare(strict_types=1);

namespace slapdash\demo\page\notFound\service\translationBuilder;

use slapdash\demo\page\notFound\library\translation\DemoNotFoundTranslationItf;
use slapdash\core\library\language\CoreLanguageItf;
use Exception;
use slapdash\demo\page\notFound\library\translation\ca\DemoNotFoundCaTranslation;
use slapdash\demo\page\notFound\library\translation\en\DemoNotFoundEnTranslation;
use slapdash\demo\page\notFound\library\translation\es\DemoNotFoundEsTranslation;
use slapdash\core\library\language\Ca;
use slapdash\core\library\language\En;
use slapdash\core\library\language\Es;

final class DemoNotFoundTranslationBuilder
{

	public static function build(CoreLanguageItf $language):DemoNotFoundTranslationItf
	{
		switch ($language->getCode())
		{
			case En::CODE:
				return new DemoNotFoundEnTranslation();
			case Es::CODE:
				return new DemoNotFoundEsTranslation();
			case Ca::CODE:
				return new DemoNotFoundCaTranslation();
		}
		throw new Exception('The language code '.$language->getCode().' is not valid.');
	}

}