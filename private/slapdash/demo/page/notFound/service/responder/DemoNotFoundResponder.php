<?php
declare(strict_types=1);

namespace slapdash\demo\page\notFound\service\responder;

use slapdash\demo\library\responder\DemoResponderAbs;
use slapdash\demo\page\notFound\library\translation\DemoNotFoundTranslationItf;
use slapdash\demo\page\notFound\service\translationContainer\DemoNotFoundTranslationContainer;
use slapdash\demo\page\notFound\service\urlBuilder\DemoNotFoundUrlBuilder;
use slapdash\demo\page\notFound\service\widget\content\DemoNotFoundContentWidget;
use slapdash\demo\page\notFound\service\widget\content\DemoNotFoundContentWidgetContext;
use slapdash\core\library\CoreHttpStatusConstant;
use slapdash\core\library\language\CoreLanguageItf;

final class DemoNotFoundResponder extends DemoResponderAbs
{

	public static function run():void
	{
		static::respond();
	}

	protected static function getPageCode():string
	{
		return 'demo-not_found';
	}

	protected static function buildTranslationAnchorElements(): array
	{
		$translationAnchorElements = [];

		foreach (static::getEnabledLanguages() as $language)
		{
			$url = DemoNotFoundUrlBuilder::buildPathByLanguage($language);

			$translationAnchorElements = static::addTranslationAnchorElement(
				$translationAnchorElements,
				$language,
				$url
			);
		}

		return $translationAnchorElements;
	}

	protected static function buildMainContent():string
	{
		$context = new DemoNotFoundContentWidgetContext(
			self::getTranslation()
		);

		return DemoNotFoundContentWidget::render($context);
	}

		private static function getTranslation():DemoNotFoundTranslationItf
		{
			return DemoNotFoundTranslationContainer::get();
		}

	protected static function getPageTitle(): string
	{
		$translation = self::getTranslation();
		return $translation->getPageTitle();
	}

	protected static function getHttpStatus():int
	{
		return CoreHttpStatusConstant::NOT_FOUND;
	}

}