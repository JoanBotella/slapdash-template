<?php
declare(strict_types=1);

namespace slapdash\demo\page\notFound\service\widget\content;

use slapdash\demo\page\notFound\library\translation\DemoNotFoundTranslationItf;

final class DemoNotFoundContentWidgetContext
{

	private DemoNotFoundTranslationItf $translation;

	public function __construct(
		DemoNotFoundTranslationItf $translation
	)
	{
		$this->translation = $translation;
	}

	public function getTranslation():DemoNotFoundTranslationItf
	{
		return $this->translation;
	}

}