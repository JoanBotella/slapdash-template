<?php
declare(strict_types=1);

namespace slapdash\demo\page\notFound\service\widget\content;

use slapdash\demo\page\notFound\service\widget\content\DemoNotFoundContentWidgetContext;

final class DemoNotFoundContentWidget
{

	public static function render(DemoNotFoundContentWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/template.php';
		return ob_get_clean();
	}

}