<?php
declare(strict_types=1);

use slapdash\demo\page\notFound\service\widget\content\DemoNotFoundContentWidgetContext;

/**
 * @var DemoNotFoundContentWidgetContext $context
 */

?>

<h1><?= $context->getTranslation()->getPageTitle() ?></h1>