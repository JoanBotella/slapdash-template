<?php
declare(strict_types=1);

namespace slapdash\demo\page\notFound\service\translationContainer;

use slapdash\demo\page\notFound\library\translation\DemoNotFoundTranslationItf;
use slapdash\demo\page\notFound\service\translationBuilder\DemoNotFoundTranslationBuilder;
use slapdash\core\library\language\CoreLanguageItf;
use slapdash\core\service\languageContainer\CoreLanguageContainer;

final class DemoNotFoundTranslationContainer
{

	private static array $translations;

	public static function get():DemoNotFoundTranslationItf
	{
		return self::getByLanguage(
			CoreLanguageContainer::get()
		);
	}

	public static function getByLanguage(CoreLanguageItf $language):DemoNotFoundTranslationItf
	{
		$languageCode = $language->getCode();
		if (!isset(self::$translations[$languageCode]))
		{
			self::$translations[$languageCode] = self::buildTranslation($language);
		}
		return self::$translations[$languageCode];
	}

		private static function buildTranslation(CoreLanguageItf $language):DemoNotFoundTranslationItf
		{
			return DemoNotFoundTranslationBuilder::build(
				$language
			);
		}

}