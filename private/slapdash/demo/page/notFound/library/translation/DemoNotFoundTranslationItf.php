<?php
declare(strict_types=1);

namespace slapdash\demo\page\notFound\library\translation;

use slapdash\core\library\translation\CorePageTranslationItf;

interface DemoNotFoundTranslationItf extends CorePageTranslationItf
{
}