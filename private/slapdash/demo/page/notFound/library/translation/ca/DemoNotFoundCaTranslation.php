<?php
declare(strict_types=1);

namespace slapdash\demo\page\notFound\library\translation\ca;

use slapdash\demo\page\notFound\library\translation\DemoNotFoundTranslationItf;

final class DemoNotFoundCaTranslation implements DemoNotFoundTranslationItf
{

	public function getPageSlug():string { return 'no-trobat'; }

	public function getPageTitle():string { return 'No Trobat'; }

}