<?php
declare(strict_types=1);

namespace slapdash\demo\page\notFound\library\translation\en;

use slapdash\demo\page\notFound\library\translation\DemoNotFoundTranslationItf;

final class DemoNotFoundEnTranslation implements DemoNotFoundTranslationItf
{

	public function getPageSlug():string { return 'not-found'; }

	public function getPageTitle():string { return 'Not Found'; }

}