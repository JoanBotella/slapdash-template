<?php
declare(strict_types=1);

namespace slapdash\demo\page\notFound\library\translation\es;

use slapdash\demo\page\notFound\library\translation\DemoNotFoundTranslationItf;

final class DemoNotFoundEsTranslation implements DemoNotFoundTranslationItf
{

	public function getPageSlug():string { return 'no-encontrado'; }

	public function getPageTitle():string { return 'No Encontrado'; }

}