<?php
declare(strict_types=1);

namespace slapdash\demo\page\notFound\library\route;

use slapdash\demo\page\notFound\library\controller\DemoNotFoundController;
use slapdash\demo\page\notFound\library\requestMatcher\DemoNotFoundRequestMatcher;
use slapdash\core\library\controller\CoreControllerItf;
use slapdash\core\library\requestMatcher\CoreRequestMatcherItf;
use slapdash\core\library\route\CoreRouteItf;

final class DemoNotFoundRoute implements CoreRouteItf
{

	public function buildController():CoreControllerItf
	{
		return new DemoNotFoundController();
	}

	public function buildRequestMatcher():CoreRequestMatcherItf
	{
		return new DemoNotFoundRequestMatcher();
	}

}