<?php
declare(strict_types=1);

namespace slapdash\demo\page\notFound\library\requestMatcher;

use slapdash\core\library\requestMatcher\CoreRequestMatcherAbs;

final class DemoNotFoundRequestMatcher extends CoreRequestMatcherAbs
{

	public function matches():bool
	{
		return true;
	}

}