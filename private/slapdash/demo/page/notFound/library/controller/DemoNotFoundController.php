<?php
declare(strict_types=1);

namespace slapdash\demo\page\notFound\library\controller;

use slapdash\demo\library\controller\DemoControllerAbs;
use slapdash\demo\page\notFound\service\responder\DemoNotFoundResponder;

final class DemoNotFoundController extends DemoControllerAbs
{

	public function action():void
	{
		DemoNotFoundResponder::run();
	}

}
