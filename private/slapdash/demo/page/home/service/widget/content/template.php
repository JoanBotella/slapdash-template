<?php
declare(strict_types=1);

use slapdash\demo\page\home\service\widget\content\DemoHomeContentWidgetContext;

/**
 * @var DemoHomeContentWidgetContext $context
 */

?>

<h1><?= $context->getTranslation()->getPageTitle() ?></h1>