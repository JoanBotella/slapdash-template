<?php
declare(strict_types=1);

namespace slapdash\demo\page\home\service\widget\content;

use slapdash\demo\page\home\service\widget\content\DemoHomeContentWidgetContext;

final class DemoHomeContentWidget
{

	public static function render(DemoHomeContentWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/template.php';
		return ob_get_clean();
	}

}