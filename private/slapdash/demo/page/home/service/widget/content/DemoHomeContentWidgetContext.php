<?php
declare(strict_types=1);

namespace slapdash\demo\page\home\service\widget\content;

use slapdash\demo\page\home\library\translation\DemoHomeTranslationItf;

final class DemoHomeContentWidgetContext
{

	private DemoHomeTranslationItf $translation;

	public function __construct(
		DemoHomeTranslationItf $translation
	)
	{
		$this->translation = $translation;
	}

	public function getTranslation():DemoHomeTranslationItf
	{
		return $this->translation;
	}

}