<?php
declare(strict_types=1);

namespace slapdash\demo\page\home\service\responder;

use slapdash\demo\library\responder\DemoResponderAbs;
use slapdash\demo\page\home\library\translation\DemoHomeTranslationItf;
use slapdash\demo\page\home\service\translationContainer\DemoHomeTranslationContainer;
use slapdash\demo\page\home\service\urlBuilder\DemoHomeUrlBuilder;
use slapdash\demo\page\home\service\widget\content\DemoHomeContentWidget;
use slapdash\demo\page\home\service\widget\content\DemoHomeContentWidgetContext;

final class DemoHomeResponder extends DemoResponderAbs
{

	public static function run():void
	{
		static::respond();
	}

	protected static function getPageCode():string
	{
		return 'demo-home';
	}

	protected static function buildTranslationAnchorElements(): array
	{
		$translationAnchorElements = [];

		foreach (static::getEnabledLanguages() as $language)
		{
			$url = DemoHomeUrlBuilder::buildPathByLanguage($language);

			$translationAnchorElements = static::addTranslationAnchorElement(
				$translationAnchorElements,
				$language,
				$url
			);
		}

		return $translationAnchorElements;
	}

	protected static function buildMainContent():string
	{
		$context = new DemoHomeContentWidgetContext(
			self::getTranslation()
		);

		return DemoHomeContentWidget::render($context);
	}

		private static function getTranslation():DemoHomeTranslationItf
		{
			return DemoHomeTranslationContainer::get();
		}

	protected static function getPageTitle(): string
	{
		$translation = self::getTranslation();
		return $translation->getPageTitle();
	}

}