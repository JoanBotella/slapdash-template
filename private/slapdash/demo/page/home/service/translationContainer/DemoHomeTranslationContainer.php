<?php
declare(strict_types=1);

namespace slapdash\demo\page\home\service\translationContainer;

use slapdash\demo\page\home\library\translation\DemoHomeTranslationItf;
use slapdash\demo\page\home\service\translationBuilder\DemoHomeTranslationBuilder;
use slapdash\core\library\language\CoreLanguageItf;
use slapdash\core\service\languageContainer\CoreLanguageContainer;

final class DemoHomeTranslationContainer
{

	private static array $translations;

	public static function get():DemoHomeTranslationItf
	{
		return self::getByLanguage(
			CoreLanguageContainer::get()
		);
	}

	public static function getByLanguage(CoreLanguageItf $language):DemoHomeTranslationItf
	{
		$languageCode = $language->getCode();
		if (!isset(self::$translations[$languageCode]))
		{
			self::$translations[$languageCode] = self::buildTranslation($language);
		}
		return self::$translations[$languageCode];
	}

		private static function buildTranslation(CoreLanguageItf $language):DemoHomeTranslationItf
		{
			return DemoHomeTranslationBuilder::build(
				$language
			);
		}

}