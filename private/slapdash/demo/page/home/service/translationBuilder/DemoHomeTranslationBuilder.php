<?php
declare(strict_types=1);

namespace slapdash\demo\page\home\service\translationBuilder;

use slapdash\demo\page\home\library\translation\DemoHomeTranslationItf;
use slapdash\core\library\language\CoreLanguageItf;
use Exception;
use slapdash\demo\page\home\library\translation\ca\DemoHomeCaTranslation;
use slapdash\demo\page\home\library\translation\en\DemoHomeEnTranslation;
use slapdash\demo\page\home\library\translation\es\DemoHomeEsTranslation;
use slapdash\core\library\language\Ca;
use slapdash\core\library\language\En;
use slapdash\core\library\language\Es;

final class DemoHomeTranslationBuilder
{

	public static function build(CoreLanguageItf $language):DemoHomeTranslationItf
	{
		switch ($language->getCode())
		{
			case En::CODE:
				return new DemoHomeEnTranslation();
			case Es::CODE:
				return new DemoHomeEsTranslation();
			case Ca::CODE:
				return new DemoHomeCaTranslation();
		}
		throw new Exception('The language code '.$language->getCode().' is not valid.');
	}

}