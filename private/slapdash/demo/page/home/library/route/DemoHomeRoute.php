<?php
declare(strict_types=1);

namespace slapdash\demo\page\home\library\route;

use slapdash\demo\page\home\library\controller\DemoHomeController;
use slapdash\demo\page\home\library\requestMatcher\DemoHomeRequestMatcher;
use slapdash\core\library\controller\CoreControllerItf;
use slapdash\core\library\requestMatcher\CoreRequestMatcherItf;
use slapdash\core\library\route\CoreRouteItf;

final class DemoHomeRoute implements CoreRouteItf
{

	public function buildController():CoreControllerItf
	{
		return new DemoHomeController();
	}

	public function buildRequestMatcher():CoreRequestMatcherItf
	{
		return new DemoHomeRequestMatcher();
	}

}