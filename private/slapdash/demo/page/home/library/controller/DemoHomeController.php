<?php
declare(strict_types=1);

namespace slapdash\demo\page\home\library\controller;

use slapdash\demo\library\controller\DemoControllerAbs;
use slapdash\demo\page\home\service\responder\DemoHomeResponder;

final class DemoHomeController extends DemoControllerAbs
{

	public function action():void
	{
		DemoHomeResponder::run();
	}

}
