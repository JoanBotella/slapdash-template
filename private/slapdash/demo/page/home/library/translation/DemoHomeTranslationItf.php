<?php
declare(strict_types=1);

namespace slapdash\demo\page\home\library\translation;

use slapdash\core\library\translation\CorePageTranslationItf;

interface DemoHomeTranslationItf extends CorePageTranslationItf
{
}