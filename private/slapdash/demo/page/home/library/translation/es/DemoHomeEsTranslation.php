<?php
declare(strict_types=1);

namespace slapdash\demo\page\home\library\translation\es;

use slapdash\demo\page\home\library\translation\DemoHomeTranslationItf;

final class DemoHomeEsTranslation implements DemoHomeTranslationItf
{

	public function getPageSlug():string { return 'inicio'; }

	public function getPageTitle():string { return 'Inicio'; }

}