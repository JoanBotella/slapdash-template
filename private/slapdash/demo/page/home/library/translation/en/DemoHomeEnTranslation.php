<?php
declare(strict_types=1);

namespace slapdash\demo\page\home\library\translation\en;

use slapdash\demo\page\home\library\translation\DemoHomeTranslationItf;

final class DemoHomeEnTranslation implements DemoHomeTranslationItf
{

	public function getPageSlug():string { return 'home'; }

	public function getPageTitle():string { return 'Home'; }

}