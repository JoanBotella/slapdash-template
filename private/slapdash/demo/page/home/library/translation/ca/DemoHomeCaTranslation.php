<?php
declare(strict_types=1);

namespace slapdash\demo\page\home\library\translation\ca;

use slapdash\demo\page\home\library\translation\DemoHomeTranslationItf;

final class DemoHomeCaTranslation implements DemoHomeTranslationItf
{

	public function getPageSlug():string { return 'inici'; }

	public function getPageTitle():string { return 'Inici'; }

}