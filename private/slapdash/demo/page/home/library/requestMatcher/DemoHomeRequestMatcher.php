<?php
declare(strict_types=1);

namespace slapdash\demo\page\home\library\requestMatcher;

use slapdash\demo\page\home\service\translationContainer\DemoHomeTranslationContainer;
use slapdash\core\library\requestMatcher\CoreRequestMatcherAbs;
use slapdash\core\service\languageContainer\CoreLanguageContainer;

final class DemoHomeRequestMatcher extends CoreRequestMatcherAbs
{

	public function matches():bool
	{
		$translation = DemoHomeTranslationContainer::get();
		$path = $translation->getPageSlug();

		$language = CoreLanguageContainer::get();
		$languageCode = $language->getCode();
		return
			$this->pathIs(
				''
			)
			|| $this->pathIs(
				$languageCode
			)
			|| $this->pathIs(
				$languageCode.'/'
			)
			|| $this->pathIs(
				$languageCode.'/'.$path
			)
		;
	}

}