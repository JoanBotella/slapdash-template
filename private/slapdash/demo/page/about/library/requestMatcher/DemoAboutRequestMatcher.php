<?php
declare(strict_types=1);

namespace slapdash\demo\page\about\library\requestMatcher;

use slapdash\demo\page\about\service\translationContainer\DemoAboutTranslationContainer;
use slapdash\core\library\requestMatcher\CoreRequestMatcherAbs;
use slapdash\core\service\languageContainer\CoreLanguageContainer;

final class DemoAboutRequestMatcher extends CoreRequestMatcherAbs
{

	public function matches():bool
	{
		$translation = DemoAboutTranslationContainer::get();
		$language = CoreLanguageContainer::get();
		return $this->pathIs(
			$language->getCode().'/'.$translation->getPageSlug()
		);
	}

}