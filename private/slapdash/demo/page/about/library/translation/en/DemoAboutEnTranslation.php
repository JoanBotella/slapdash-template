<?php
declare(strict_types=1);

namespace slapdash\demo\page\about\library\translation\en;

use slapdash\demo\page\about\library\translation\DemoAboutTranslationItf;

final class DemoAboutEnTranslation implements DemoAboutTranslationItf
{

	public function getPageSlug():string { return 'about'; }

	public function getPageTitle():string { return 'About'; }

}