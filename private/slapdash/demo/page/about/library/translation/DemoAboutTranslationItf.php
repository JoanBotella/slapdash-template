<?php
declare(strict_types=1);

namespace slapdash\demo\page\about\library\translation;

use slapdash\core\library\translation\CorePageTranslationItf;

interface DemoAboutTranslationItf extends CorePageTranslationItf
{
}