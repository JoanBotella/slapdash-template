<?php
declare(strict_types=1);

namespace slapdash\demo\page\about\library\translation\ca;

use slapdash\demo\page\about\library\translation\DemoAboutTranslationItf;

final class DemoAboutCaTranslation implements DemoAboutTranslationItf
{

	public function getPageSlug():string { return 'sobre'; }

	public function getPageTitle():string { return 'Sobre'; }

}