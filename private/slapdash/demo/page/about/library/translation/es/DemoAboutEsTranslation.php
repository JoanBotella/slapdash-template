<?php
declare(strict_types=1);

namespace slapdash\demo\page\about\library\translation\es;

use slapdash\demo\page\about\library\translation\DemoAboutTranslationItf;

final class DemoAboutEsTranslation implements DemoAboutTranslationItf
{

	public function getPageSlug():string { return 'sobre'; }

	public function getPageTitle():string { return 'Sobre'; }

}