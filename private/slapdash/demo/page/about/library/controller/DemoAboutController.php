<?php
declare(strict_types=1);

namespace slapdash\demo\page\about\library\controller;

use slapdash\demo\library\controller\DemoControllerAbs;
use slapdash\demo\page\about\service\responder\DemoAboutResponder;

final class DemoAboutController extends DemoControllerAbs
{

	public function action():void
	{
		DemoAboutResponder::run();
	}

}
