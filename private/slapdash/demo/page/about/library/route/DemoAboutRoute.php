<?php
declare(strict_types=1);

namespace slapdash\demo\page\about\library\route;

use slapdash\demo\page\about\library\controller\DemoAboutController;
use slapdash\demo\page\about\library\requestMatcher\DemoAboutRequestMatcher;
use slapdash\core\library\controller\CoreControllerItf;
use slapdash\core\library\requestMatcher\CoreRequestMatcherItf;
use slapdash\core\library\route\CoreRouteItf;

final class DemoAboutRoute implements CoreRouteItf
{

	public function buildController():CoreControllerItf
	{
		return new DemoAboutController();
	}

	public function buildRequestMatcher():CoreRequestMatcherItf
	{
		return new DemoAboutRequestMatcher();
	}

}