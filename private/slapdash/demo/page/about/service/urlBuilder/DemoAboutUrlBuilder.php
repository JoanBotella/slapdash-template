<?php
declare(strict_types=1);

namespace slapdash\demo\page\about\service\urlBuilder;

use slapdash\demo\page\about\service\translationContainer\DemoAboutTranslationContainer;
use slapdash\core\library\language\CoreLanguageItf;
use slapdash\core\library\urlBuilder\CoreUrlBuilderAbs;
use slapdash\core\service\languageContainer\CoreLanguageContainer;

final class DemoAboutUrlBuilder extends CoreUrlBuilderAbs
{

	public static function buildAbsolute():string
	{
		return self::buildAbsoluteByLanguage(
			CoreLanguageContainer::get()
		);
	}

	public static function buildAbsoluteByLanguage(CoreLanguageItf $language):string
	{
		return
			self::buildAppUrlProtocolAndHost()
			.'/'
			.self::buildPathByLanguage($language)
		;
	}

	public static function buildPath():string
	{
		return self::buildPathByLanguage(
			CoreLanguageContainer::get()
		);
	}

	public static function buildPathByLanguage(CoreLanguageItf $language):string
	{
		$translation = DemoAboutTranslationContainer::getByLanguage($language);
		return
			self::buildAppUrlBasePathAndLanguageSegment($language)
			.'/'
			.$translation->getPageSlug()
		;
	}

}