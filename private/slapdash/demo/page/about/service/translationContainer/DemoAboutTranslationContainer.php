<?php
declare(strict_types=1);

namespace slapdash\demo\page\about\service\translationContainer;

use slapdash\demo\page\about\library\translation\DemoAboutTranslationItf;
use slapdash\demo\page\about\service\translationBuilder\DemoAboutTranslationBuilder;
use slapdash\core\library\language\CoreLanguageItf;
use slapdash\core\service\languageContainer\CoreLanguageContainer;

final class DemoAboutTranslationContainer
{

	private static array $translations;

	public static function get():DemoAboutTranslationItf
	{
		return self::getByLanguage(
			CoreLanguageContainer::get()
		);
	}

	public static function getByLanguage(CoreLanguageItf $language):DemoAboutTranslationItf
	{
		$languageCode = $language->getCode();
		if (!isset(self::$translations[$languageCode]))
		{
			self::$translations[$languageCode] = self::buildTranslation($language);
		}
		return self::$translations[$languageCode];
	}

		private static function buildTranslation(CoreLanguageItf $language):DemoAboutTranslationItf
		{
			return DemoAboutTranslationBuilder::build(
				$language
			);
		}

}