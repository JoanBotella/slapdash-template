<?php
declare(strict_types=1);

namespace slapdash\demo\page\about\service\widget\content;

use slapdash\demo\page\about\service\widget\content\DemoAboutContentWidgetContext;

final class DemoAboutContentWidget
{

	public static function render(DemoAboutContentWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/template.php';
		return ob_get_clean();
	}

}