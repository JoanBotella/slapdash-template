<?php
declare(strict_types=1);

use slapdash\demo\page\about\service\widget\content\DemoAboutContentWidgetContext;

/**
 * @var DemoAboutContentWidgetContext $context
 */

?>

<h1><?= $context->getTranslation()->getPageTitle() ?></h1>