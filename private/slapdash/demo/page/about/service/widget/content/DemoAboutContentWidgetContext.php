<?php
declare(strict_types=1);

namespace slapdash\demo\page\about\service\widget\content;

use slapdash\demo\page\about\library\translation\DemoAboutTranslationItf;

final class DemoAboutContentWidgetContext
{

	private DemoAboutTranslationItf $translation;

	public function __construct(
		DemoAboutTranslationItf $translation
	)
	{
		$this->translation = $translation;
	}

	public function getTranslation():DemoAboutTranslationItf
	{
		return $this->translation;
	}

}