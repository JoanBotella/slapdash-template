<?php
declare(strict_types=1);

namespace slapdash\demo\page\about\service\responder;

use slapdash\demo\library\responder\DemoResponderAbs;
use slapdash\demo\page\about\library\translation\DemoAboutTranslationItf;
use slapdash\demo\page\about\service\translationContainer\DemoAboutTranslationContainer;
use slapdash\demo\page\about\service\urlBuilder\DemoAboutUrlBuilder;
use slapdash\demo\page\about\service\widget\content\DemoAboutContentWidget;
use slapdash\demo\page\about\service\widget\content\DemoAboutContentWidgetContext;

final class DemoAboutResponder extends DemoResponderAbs
{

	public static function run():void
	{
		static::respond();
	}

	protected static function getPageCode():string
	{
		return 'demo-about';
	}

	protected static function buildTranslationAnchorElements(): array
	{
		$translationAnchorElements = [];

		foreach (static::getEnabledLanguages() as $language)
		{
			$url = DemoAboutUrlBuilder::buildPathByLanguage($language);

			$translationAnchorElements = static::addTranslationAnchorElement(
				$translationAnchorElements,
				$language,
				$url
			);
		}

		return $translationAnchorElements;
	}

	protected static function buildMainContent():string
	{
		$context = new DemoAboutContentWidgetContext(
			self::getTranslation()
		);

		return DemoAboutContentWidget::render($context);
	}

		private static function getTranslation():DemoAboutTranslationItf
		{
			return DemoAboutTranslationContainer::get();
		}

	protected static function getPageTitle(): string
	{
		$translation = self::getTranslation();
		return $translation->getPageTitle();
	}

}