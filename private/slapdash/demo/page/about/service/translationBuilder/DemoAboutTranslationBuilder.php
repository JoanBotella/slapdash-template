<?php
declare(strict_types=1);

namespace slapdash\demo\page\about\service\translationBuilder;

use slapdash\demo\page\about\library\translation\DemoAboutTranslationItf;
use slapdash\core\library\language\CoreLanguageItf;
use Exception;
use slapdash\demo\page\about\library\translation\ca\DemoAboutCaTranslation;
use slapdash\demo\page\about\library\translation\en\DemoAboutEnTranslation;
use slapdash\demo\page\about\library\translation\es\DemoAboutEsTranslation;
use slapdash\core\library\language\Ca;
use slapdash\core\library\language\En;
use slapdash\core\library\language\Es;

final class DemoAboutTranslationBuilder
{

	public static function build(CoreLanguageItf $language):DemoAboutTranslationItf
	{
		switch ($language->getCode())
		{
			case En::CODE:
				return new DemoAboutEnTranslation();
			case Es::CODE:
				return new DemoAboutEsTranslation();
			case Ca::CODE:
				return new DemoAboutCaTranslation();
		}
		throw new Exception('The language code '.$language->getCode().' is not valid.');
	}

}