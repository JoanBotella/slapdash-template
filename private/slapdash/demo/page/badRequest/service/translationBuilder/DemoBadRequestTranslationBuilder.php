<?php
declare(strict_types=1);

namespace slapdash\demo\page\badRequest\service\translationBuilder;

use slapdash\demo\page\badRequest\library\translation\DemoBadRequestTranslationItf;
use slapdash\core\library\language\CoreLanguageItf;
use Exception;
use slapdash\demo\page\badRequest\library\translation\ca\DemoBadRequestCaTranslation;
use slapdash\demo\page\badRequest\library\translation\en\DemoBadRequestEnTranslation;
use slapdash\demo\page\badRequest\library\translation\es\DemoBadRequestEsTranslation;
use slapdash\core\library\language\Ca;
use slapdash\core\library\language\En;
use slapdash\core\library\language\Es;

final class DemoBadRequestTranslationBuilder
{

	public static function build(CoreLanguageItf $language):DemoBadRequestTranslationItf
	{
		switch ($language->getCode())
		{
			case En::CODE:
				return new DemoBadRequestEnTranslation();
			case Es::CODE:
				return new DemoBadRequestEsTranslation();
			case Ca::CODE:
				return new DemoBadRequestCaTranslation();
		}
		throw new Exception('The language code '.$language->getCode().' is not valid.');
	}

}