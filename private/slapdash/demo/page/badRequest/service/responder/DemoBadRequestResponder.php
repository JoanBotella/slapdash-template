<?php
declare(strict_types=1);

namespace slapdash\demo\page\badRequest\service\responder;

use slapdash\demo\library\responder\DemoResponderAbs;
use slapdash\demo\page\badRequest\library\translation\DemoBadRequestTranslationItf;
use slapdash\demo\page\badRequest\service\translationContainer\DemoBadRequestTranslationContainer;
use slapdash\demo\page\badRequest\service\urlBuilder\DemoBadRequestUrlBuilder;
use slapdash\demo\page\badRequest\service\widget\content\DemoBadRequestContentWidget;
use slapdash\demo\page\badRequest\service\widget\content\DemoBadRequestContentWidgetContext;
use slapdash\core\library\CoreHttpStatusConstant;

final class DemoBadRequestResponder extends DemoResponderAbs
{

	public static function run():void
	{
		static::respond();
	}

	protected static function getPageCode():string
	{
		return 'demo-bad_request';
	}

	protected static function buildTranslationAnchorElements(): array
	{
		$translationAnchorElements = [];

		foreach (static::getEnabledLanguages() as $language)
		{
			$url = DemoBadRequestUrlBuilder::buildPathByLanguage($language);

			$translationAnchorElements = static::addTranslationAnchorElement(
				$translationAnchorElements,
				$language,
				$url
			);
		}

		return $translationAnchorElements;
	}

	protected static function buildMainContent():string
	{
		$context = new DemoBadRequestContentWidgetContext(
			self::getTranslation()
		);

		return DemoBadRequestContentWidget::render($context);
	}

		private static function getTranslation():DemoBadRequestTranslationItf
		{
			return DemoBadRequestTranslationContainer::get();
		}

	protected static function getPageTitle(): string
	{
		$translation = self::getTranslation();
		return $translation->getPageTitle();
	}

	protected static function getHttpStatus():int
	{
		return CoreHttpStatusConstant::BAD_REQUEST;
	}

}