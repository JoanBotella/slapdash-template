<?php
declare(strict_types=1);

namespace slapdash\demo\page\badRequest\service\translationContainer;

use slapdash\demo\page\badRequest\library\translation\DemoBadRequestTranslationItf;
use slapdash\demo\page\badRequest\service\translationBuilder\DemoBadRequestTranslationBuilder;
use slapdash\core\library\language\CoreLanguageItf;
use slapdash\core\service\languageContainer\CoreLanguageContainer;

final class DemoBadRequestTranslationContainer
{

	private static array $translations;

	public static function get():DemoBadRequestTranslationItf
	{
		return self::getByLanguage(
			CoreLanguageContainer::get()
		);
	}

	public static function getByLanguage(CoreLanguageItf $language):DemoBadRequestTranslationItf
	{
		$languageCode = $language->getCode();
		if (!isset(self::$translations[$languageCode]))
		{
			self::$translations[$languageCode] = self::buildTranslation($language);
		}
		return self::$translations[$languageCode];
	}

		private static function buildTranslation(CoreLanguageItf $language):DemoBadRequestTranslationItf
		{
			return DemoBadRequestTranslationBuilder::build(
				$language
			);
		}

}