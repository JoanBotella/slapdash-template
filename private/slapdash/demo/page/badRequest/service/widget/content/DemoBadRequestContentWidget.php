<?php
declare(strict_types=1);

namespace slapdash\demo\page\badRequest\service\widget\content;

use slapdash\demo\page\badRequest\service\widget\content\DemoBadRequestContentWidgetContext;

final class DemoBadRequestContentWidget
{

	public static function render(DemoBadRequestContentWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/template.php';
		return ob_get_clean();
	}

}