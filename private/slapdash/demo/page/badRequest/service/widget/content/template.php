<?php
declare(strict_types=1);

use slapdash\demo\page\badRequest\service\widget\content\DemoBadRequestContentWidgetContext;

/**
 * @var DemoBadRequestContentWidgetContext $context
 */

?>

<h1><?= $context->getTranslation()->getPageTitle() ?></h1>