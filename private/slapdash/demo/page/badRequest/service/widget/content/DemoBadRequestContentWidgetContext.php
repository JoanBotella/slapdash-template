<?php
declare(strict_types=1);

namespace slapdash\demo\page\badRequest\service\widget\content;

use slapdash\demo\page\badRequest\library\translation\DemoBadRequestTranslationItf;

final class DemoBadRequestContentWidgetContext
{

	private DemoBadRequestTranslationItf $translation;

	public function __construct(
		DemoBadRequestTranslationItf $translation
	)
	{
		$this->translation = $translation;
	}

	public function getTranslation():DemoBadRequestTranslationItf
	{
		return $this->translation;
	}

}