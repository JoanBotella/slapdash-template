<?php
declare(strict_types=1);

namespace slapdash\demo\page\badRequest\library\route;

use slapdash\demo\page\badRequest\library\controller\DemoBadRequestController;
use slapdash\demo\page\badRequest\library\requestMatcher\DemoBadRequestRequestMatcher;
use slapdash\core\library\controller\CoreControllerItf;
use slapdash\core\library\requestMatcher\CoreRequestMatcherItf;
use slapdash\core\library\route\CoreRouteItf;

final class DemoBadRequestRoute implements CoreRouteItf
{

	public function buildController():CoreControllerItf
	{
		return new DemoBadRequestController();
	}

	public function buildRequestMatcher():CoreRequestMatcherItf
	{
		return new DemoBadRequestRequestMatcher();
	}

}