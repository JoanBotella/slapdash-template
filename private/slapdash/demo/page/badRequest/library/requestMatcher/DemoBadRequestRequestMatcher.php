<?php
declare(strict_types=1);

namespace slapdash\demo\page\badRequest\library\requestMatcher;

use slapdash\demo\page\badRequest\service\translationContainer\DemoBadRequestTranslationContainer;
use slapdash\core\library\requestMatcher\CoreRequestMatcherAbs;
use slapdash\core\service\languageContainer\CoreLanguageContainer;

final class DemoBadRequestRequestMatcher extends CoreRequestMatcherAbs
{

	public function matches():bool
	{
		$translation = DemoBadRequestTranslationContainer::get();
		$language = CoreLanguageContainer::get();
		return $this->pathIs(
			$language->getCode().'/'.$translation->getPageSlug()
		);
	}

}