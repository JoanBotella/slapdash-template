<?php
declare(strict_types=1);

namespace slapdash\demo\page\badRequest\library\controller;

use slapdash\demo\library\controller\DemoControllerAbs;
use slapdash\demo\page\badRequest\service\responder\DemoBadRequestResponder;

final class DemoBadRequestController extends DemoControllerAbs
{

	public function action():void
	{
		DemoBadRequestResponder::run();
	}

}
