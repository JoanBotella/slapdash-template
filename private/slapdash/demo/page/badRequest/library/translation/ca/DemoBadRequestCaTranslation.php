<?php
declare(strict_types=1);

namespace slapdash\demo\page\badRequest\library\translation\ca;

use slapdash\demo\page\badRequest\library\translation\DemoBadRequestTranslationItf;

final class DemoBadRequestCaTranslation implements DemoBadRequestTranslationItf
{

	public function getPageSlug():string { return 'peticio-incorrecta'; }

	public function getPageTitle():string { return 'Petició Incorrecta'; }

}