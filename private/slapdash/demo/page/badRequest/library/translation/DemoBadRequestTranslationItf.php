<?php
declare(strict_types=1);

namespace slapdash\demo\page\badRequest\library\translation;

use slapdash\core\library\translation\CorePageTranslationItf;

interface DemoBadRequestTranslationItf extends CorePageTranslationItf
{
}