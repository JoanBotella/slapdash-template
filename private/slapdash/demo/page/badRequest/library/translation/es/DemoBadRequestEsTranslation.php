<?php
declare(strict_types=1);

namespace slapdash\demo\page\badRequest\library\translation\es;

use slapdash\demo\page\badRequest\library\translation\DemoBadRequestTranslationItf;

final class DemoBadRequestEsTranslation implements DemoBadRequestTranslationItf
{

	public function getPageSlug():string { return 'peticion-incorrecta'; }

	public function getPageTitle():string { return 'Petición Incorrecta'; }

}