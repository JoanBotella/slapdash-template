<?php
declare(strict_types=1);

namespace slapdash\demo\page\badRequest\library\translation\en;

use slapdash\demo\page\badRequest\library\translation\DemoBadRequestTranslationItf;

final class DemoBadRequestEnTranslation implements DemoBadRequestTranslationItf
{

	public function getPageSlug():string { return 'bad-request'; }

	public function getPageTitle():string { return 'Bad Request'; }

}